import 'package:filemanager/bootstrap.dart';
import 'package:filemanager/views/widgets/drawer_widget.dart';
import 'package:path/path.dart';
import 'dart:async';
import 'package:root_access/root_access.dart';

class RootPage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      drawer: DrawerWidget(),
      appBar: AppBar(
        title: Text("Akses Root"),
        backgroundColor: Colors.red,
      ),
      body: _Root(),
    );
  }
}

class _Root extends StatefulWidget {
  @override
  __RootState createState() => new __RootState();
}

class __RootState extends State<_Root> {
  bool _rootAccess = false;

  @override
  void initState() {
    super.initState();
    initRootRequest();
  }

  Future<void> initRootRequest() async {
    bool rootAccess = await RootAccess.requestRootAccess;

    if (!mounted) return;

    setState(() {
      _rootAccess = rootAccess;
    });
  }

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
        debugShowCheckedModeBanner: false,
        home: Scaffold(
          body: Center(
              child: Text(
            'Mengizinkan akses ke sistem, berbahaya bagi perangkatmu\n apa pun yang terjadi, aku takkan peduli :D\n\n\n Root akses : $_rootAccess\n',
            textAlign: TextAlign.center,
            style: TextStyle(fontSize: 24, color: Colors.cyanAccent),
          )),
          backgroundColor: Colors.black,
        ));
  }
}
